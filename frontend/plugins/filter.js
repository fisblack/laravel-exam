import Vue from 'vue'

export default ({ app }) => {
  Vue.filter('numberFormat', (value) => {
    if (!isNaN(value)) {
      return Number(value).toLocaleString()
    }
  })
}
