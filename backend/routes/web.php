<?php

use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\DashboardController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

// Route::get('/', function () {
//     return view('welcome');
// });

Route::prefix('admin')->middleware('guest')->name('admin.')->group(function () {
    Route::get('login', array(AuthController::class, 'showLogin'))->name('showLogin');
    Route::post('login', array(AuthController::class, 'login'))->name('login');
});

Route::prefix('admin')->middleware('auth')->name('admin.')->group(function () {
    Route::get('/logout', array(AuthController::class, 'logout'))->name('logout');
    Route::get('/', array(DashboardController::class, 'index'))->name('dashboard');
    Route::get('/logs/{id}', array(DashboardController::class, 'logs'))->name('logs');
    Route::get('/member/{id}/edit', array(DashboardController::class, 'edit'))->name('edit');
});
