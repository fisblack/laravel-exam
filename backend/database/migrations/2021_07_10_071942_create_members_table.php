<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->id();
            $table->string('firstNameTH');
            $table->string('lastNameTH');
            $table->string('firstNameEN');
            $table->string('lastNameEN');
            $table->enum('gender', ['male', 'female']);
            $table->text('address');
            $table->string('contactNumber', 15);
            $table->string('email');
            $table->bigInteger('idCard');
            $table->double('income', 8, 2)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
