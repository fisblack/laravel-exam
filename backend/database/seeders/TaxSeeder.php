<?php

namespace Database\Seeders;

use App\Models\Tax;
use Illuminate\Database\Seeder;

class TaxSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tax::insert(array(
            array(
                'min'       => 0,
                'max'       => 150000,
                'maxIncome' => 150000,
                'taxRate'   => 5,
            ),
            array(
                'min'       => 150001,
                'max'       => 300000,
                'maxIncome' => 150000,
                'taxRate'   => 5,
            ),
            array(
                'min'       => 300001,
                'max'       => 500000,
                'maxIncome' => 200000,
                'taxRate'   => 10,
            ),
            array(
                'min'       => 500001,
                'max'       => 750000,
                'maxIncome' => 250000,
                'taxRate'   => 15,
            ),
            array(
                'min'       => 750001,
                'max'       => 1000000,
                'maxIncome' => 250000,
                'taxRate'   => 20,
            ),
            array(
                'min'       => 1000001,
                'max'       => 2000000,
                'maxIncome' => 1000000,
                'taxRate'   => 25,
            ),
            array(
                'min'       => 2000001,
                'max'       => 5000000,
                'maxIncome' => 3000000,
                'taxRate'   => 30,
            ),
            array(
                'min'       => 5000001,
                'max'       => null,
                'maxIncome' => null,
                'taxRate'   => 35,
            ),
        ));
    }
}
