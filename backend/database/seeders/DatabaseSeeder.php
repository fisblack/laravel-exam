<?php

namespace Database\Seeders;

use Database\Seeders\AuthSeeder;
use Database\Seeders\TaxSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(array(
            AuthSeeder::class,
            TaxSeeder::class,
        ));
        // \App\Models\User::factory(10)->create();
    }
}
