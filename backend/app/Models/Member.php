<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    use HasFactory;

    protected $fillable = array(
        'firstNameTH',
        'lastNameTH',
        'firstNameEN',
        'lastNameEN',
        'gender',
        'address',
        'contactNumber',
        'email',
        'idCard',
        'income',
    );

    public function logs()
    {
        return $this->hasMany(Logging::class);
    }

}
