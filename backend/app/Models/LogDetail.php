<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogDetail extends Model
{
    use HasFactory;

    protected $fillable = array(
        'logs_id',
        'fields',
        'original',
        'old',
    );
}
