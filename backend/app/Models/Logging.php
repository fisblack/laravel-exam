<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Logging extends Model
{
    use HasFactory;

    protected $fillable = array(
        'member_id',
    );

    public function detail()
    {
        return $this->hasMany(LogDetail::class, 'logs_id', 'id');
    }

    public function actionBy()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
