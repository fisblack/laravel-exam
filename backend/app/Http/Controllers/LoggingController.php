<?php

namespace App\Http\Controllers;

use App\Models\LogDetail;
use App\Models\Logging;

class LoggingController extends Controller
{
    public static function create($old, $new)
    {
        $log = '';

        foreach ($new as $key => $value) {
            $new[strtolower($key)] = $value;
        }

        foreach ($old as $key => $value) {
            if (isset($new[strtolower($key)])) {

                $new[$key] = Self::changeType($value, $new[$key]);

                if ($new[$key] !== $value) {

                    if (empty($log)) {
                        $log = new Logging();
                        $log->user_id = $new['actionBy'] ?? null;
                        $log->member_id = $old['id'];
                        $log->save();
                    }

                    LogDetail::create(array(
                        'logs_id'  => $log->id,
                        'fields'   => $key,
                        'original' => $new[$key],
                        'old'      => $value,
                    ));
                }
            }
        }
    }

    private static function changeType($original, $new)
    {
        switch (gettype($original)) {
            case 'integer':
                return intval($new);

            case 'double':
                return floatval($new);

            default:
                return $new;
        }
    }
}
