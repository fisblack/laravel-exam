<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Member;
use App\Models\Tax;

class DashboardController extends Controller
{
    public function index()
    {
        $members = Member::all();

        foreach ($members as $key => $member) {
            $members[$key]['tax'] = Self::calculateTax($member->income);
        }

        return View('admin.dashboard', compact('members'));
    }

    private static function calculateTax($income)
    {
        $totalIncome = $income * 12;
        $taxactions = Tax::where('min', '<=', $totalIncome)->get();
        $tax = 0;

        foreach ($taxactions as $key => $taxaction) {
            if ($key == 0) {
                $tax += 0;
                $totalIncome -= $totalIncome >= $taxaction->maxIncome ? $taxaction->maxIncome : $totalIncome;
            } else {
                $maxTax = $totalIncome >= $taxaction->maxIncome ? $taxaction->maxIncome : $totalIncome;
                $tax += $maxTax / 100 * $taxaction->taxRate;
                $totalIncome -= $taxaction->maxIncome;
            }
        }

        return $tax;
    }

    public function logs($id)
    {
        $member = Member::with(array('logs.detail', 'logs.actionBy'))->findOrFail($id);

        return View('admin.logs', compact('member'));
    }

    public function edit($id)
    {
        $member = Member::findOrFail($id);

        return View('admin.edit', compact('member'));
    }
}
