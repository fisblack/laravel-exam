<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $request->validate(array(
            'email'    => 'required|email',
            'password' => 'required|min:6',
        ));

        $email = $request->input('email');
        $password = $request->input('password');
        $remember = $request->input('remember', false);

        if (Auth::attempt(array('email' => $email, 'password' => $password), $remember)) {
            return redirect()->route('admin.dashboard');
        } else {

            return back()->withInput()->with('unauthorized', 'Email Or Password inValid');

        }

    }

    public function showLogin()
    {
        return View('admin.auth.login');
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect()->route('admin.login');
    }
}
