<?php

namespace App\Http\Controllers;

use App\Http\Controllers\LoggingController as Logging;
use App\Models\Member;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $members = '';

        if (!empty($request->get('q'))) {
            $members = Member::where('firstNameTH', 'like', '%' . $request->get('q') . '')
                ->OrWhere('lastNameTH', 'like', '%' . $request->get('q') . '')
                ->OrWhere('firstNameEN', 'like', '%' . $request->get('q') . '')
                ->OrWhere('lastNameEN', 'like', '%' . $request->get('q') . '')
                ->OrWhere('gender', 'like', '%' . $request->get('q') . '')
                ->OrWhere('address', 'like', '%' . $request->get('q') . '')
                ->OrWhere('contactNumber', 'like', '%' . $request->get('q') . '')
                ->OrWhere('email', 'like', '%' . $request->get('q') . '')
                ->OrWhere('idCard', 'like', '%' . $request->get('q') . '')
                ->OrWhere('income', 'like', '%' . $request->get('q') . '')
                ->get();
        } else {
            $members = Member::all();
        }

        return response()->json(array('members' => $members));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), array(
            'firstNameTH'   => 'required|string|min:3|max:50',
            'lastNameTH'    => 'required|string|min:3|max:50',
            'firstNameEN'   => 'required|string|min:3|max:50',
            'lastNameEN'    => 'required|string|min:3|max:50',
            'gender'        => 'required', Rule::in(array('male', 'female')),
            'address'       => 'required|min:3|max:255',
            'contactNumber' => 'required|numeric|digits:10|regex:/^0[14689][0-9]{8}/',
            'email'         => 'required|email',
            'idCard'        => 'required|numeric|digits:13',
            'income'        => 'required|numeric',
        ));

        if ($validator->fails()) {
            return response()->json(array('errors' => $validator->messages()), 400);
        }

        $member = Member::create($request->input());

        return response()->json(array('member' => $member));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $member = Member::find($id);

        if (!$member) {
            return response()->json(array('_errors' => 'not found member ' . $id), 404);
        }

        return response()->json(array('member' => $member), 200);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), array(
            'firstNameTH'   => 'required|string|min:3|max:50',
            'lastNameTH'    => 'required|string|min:3|max:50',
            'firstNameEN'   => 'required|string|min:3|max:50',
            'lastNameEN'    => 'required|string|min:3|max:50',
            'gender'        => 'required', Rule::in(array('male', 'female')),
            'address'       => 'required|min:3|max:255',
            'contactNumber' => 'required|numeric|digits:10|regex:/^0[14689][0-9]{8}/',
            'email'         => 'required|email',
            'idCard'        => 'required|numeric|digits:13',
            'income'        => 'required|numeric',
        ));

        if ($validator->fails()) {
            return response()->json(array('errors' => $validator->messages()), 400);
        }

        $member = Member::find($id);

        if (!$member) {
            return response()->json(array('_errors' => 'not found member ' . $id), 404);
        }

        DB::beginTransaction();
        try {
            Logging::create($member->getRawOriginal(), $request->input());

            $member->firstNameTH = $request->firstNameTH;
            $member->lastNameTH = $request->firstNameTH;
            $member->firstNameEN = $request->firstNameEN;
            $member->lastNameTH = $request->lastNameTH;
            $member->gender = $request->gender;
            $member->address = $request->address;
            $member->contactNumber = $request->contactNumber;
            $member->email = $request->email;
            $member->idCard = $request->idCard;
            $member->income = $request->income;

            $member->save();

            DB::commit();

            return response()->json(array('member' => $member), 200);
        } catch (\Throwable$th) {
            throw $th;
            DB::rollBack();

            return response()->json(array('_errors' => 'Update fails'), 500);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $member = Member::findOrFail($id);

        $member->delete();

        return response()->json(array('status' => 'Deleted Success'), 200);
    }
}
