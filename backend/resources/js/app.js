require('./bootstrap');

import Vue from 'vue'
import moment from 'moment';
import axios from 'axios';


const files = require.context('./', true, /\.vue$/i)

files.keys().map(key => {
    Vue.component(files(key).default.name ?? key.split('/').pop().split('.')[0], files(key).default);
})

Vue.filter('numberFormat', value => {
  if (!isNaN(value)) {
    return Number(value).toLocaleString()
  }
})

Vue.filter('formatDate', (value, format = 'DD MMM YYYY HH:mm:ss') => {
  // eslint-disable-line
  if (value) {
    return moment(String(value)).format(format)
  }
})



window.Vue = Vue
window.axios = axios

//  const app = new Vue({
//      el: '#app',
//      component: { App }
//  })
