@extends('admin.layouts')

@section('content')
@verbatim
    <div class="row">
        <div class="col-lg-12 mb-4">
            <!-- Illustrations -->
            <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">
                        Members List
                    </h6>
                </div>
                <div class="card-body">
                    <base-input
                        label="Search"
                        class="col-3 float-right"
                        v-model="search"
                        @input="onFetch"
                    ></base-input>
                    <base-table :data="members" :columns="columns" label-empty="Empty Data">
                        <template slot-scope="{ row }">
                            <td scope="row">{{ row.firstNameTH }}</td>
                            <td scope="row">{{ row.lastNameTH }}</td>
                            <td scope="row">{{ row.firstNameEN }}</td>
                            <td scope="row">{{ row.lastNameEN }}</td>
                            <td scope="row">{{ row.gender }}</td>
                            <td scope="row">{{ row.address }}</td>
                            <td scope="row">{{ row.contactNumber }}</td>
                            <td scope="row">{{ row.email }}</td>
                            <td scope="row">{{ row.idCard }}</td>
                            <td scope="row">{{ row.income * 12 | numberFormat }}</td>
                            <td scope="row">{{ row.tax | numberFormat}}</td>
                            <td scope="row" class="d-flex" style="width: 230px">
                                <a :href="`/admin/logs/${row.id}`">
                                    <button type="button" class="btn btn-sm btn-primary">View Logs</button>
                                </a>
                                <a :href="`/admin/member/${row.id}/edit`">
                                    <button type="button" class="btn btn-sm btn-primary mx-2">Edit</button>
                                </a>
                                <button type="button" class="btn btn-sm btn-primary" @click="onDelete(row.id)">Delete</button>
                            </td>
                        </template>
                    </base-table>
                </div>
            </div>
        </div>
    </div>
@endverbatim
@endsection
@section('script')
<script>
    new Vue({
        el: '#app',
        data() {
            return {
                members: {!! $members->toJson() !!},
                columns: [
                    'firstNameTH',
                    'lastNameTH',
                    'firstNameEN',
                    'lastNameEN',
                    'gender',
                    'address',
                    'contactNumber',
                    'email',
                    'idCard',
                    'inCome',
                    'tax',
                    'action'
                ],
                search: '',
            }
        },
        methods: {
            async onFetch() {
                const { members } = await axios
                                            .get(`/api/member?q=${this.search}`)
                                            .then(({data}) => data);
                this.members = members
            },
            onDelete(id) {
                console.log(id);
                if(confirm('Are you sure delete')) {
                    axios
                        .delete(`/api/member/${id}`)
                        .then(() => {
                            this.onFetch()
                        })
                        .catch(({response}) => {
                            if (response) {
                                console.log(response.data);
                            }
                        })
                }
            }
        },
    })
</script>
@endsection
