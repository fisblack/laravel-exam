@extends('admin.layouts')
@section('content')
    <div class="row">
        <div class="col-lg-12 mb-4">
            <!-- Illustrations -->
            <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">
                        Edit Members
                    </h6>
                </div>
                <div class="card-body">
                    <form class="col-8 mx-auto" @submit.prevent="onUpdate">
                        <div class="row">
                            <base-input
                                class="col-6"
                                label="first Name TH"
                                v-model="member.firstNameTH"
                                :errors="errorState.firstNameTH"
                            ></base-input>
                            <base-input
                                class="col-6"
                                label="last Name TH"
                                v-model="member.lastNameTH"
                                :errors="errorState.lastNameTH"
                            ></base-input>
                        </div>
                        <div class="row">
                            <base-input
                                class="col-6"
                                label="first Name EN"
                                v-model="member.firstNameEN"
                                :errors="errorState.firstNameEN"
                            ></base-input>
                            <base-input
                                class="col-6"
                                label="last Name EN"
                                v-model="member.lastNameEN"
                                :errors="errorState.lastNameEN"
                            ></base-input>
                        </div>
                        <div class="row">
                            <base-select
                                class="col-12"
                                :options="gender"
                                v-model="member.gender"
                                label="gender"
                                :errors="errorState.gender"
                            ></base-select>
                        </div>
                        <div class="row">
                            <base-textarea
                                class="col-12"
                                v-model="member.address"
                                label="Address"
                                :errors="errorState.address"
                            ></base-textarea>
                        </div>
                        <div class="row">
                            <base-input
                                class="col-6"
                                label="contact number"
                                v-model="member.contactNumber"
                                :errors="errorState.contactNumber"
                            ></base-input>
                            <base-input
                                class="col-6"
                                label="email"
                                v-model="member.email"
                                :errors="errorState.email"
                            ></base-input>
                        </div>
                        <div class="row">
                            <base-input
                                class="col-6"
                                label="id card"
                                v-model="member.idCard"
                                :errors="errorState.idCard"
                            ></base-input>
                            <base-input
                                class="col-6"
                                label="monthly income"
                                v-model="member.income"
                                :errors="errorState.income"
                            ></base-input>
                        </div>

                        <button type="submit" class="btn btn-sm btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
<script>
    new Vue({
        el: '#app',
        data() {
            return {
                member: {!! $member->toJson() !!},
                gender: [
                    { value: 'male', label: "male" },
                    { value: 'female', label: "female" }
                ],
                errorState: {}
            }
        },
        methods: {
            onUpdate() {
                const actionBy = {!! Auth::user()->id !!}
                const data = { ...this.member, actionBy }
                delete data.created_at
                delete data.updated_at

                axios
                    .put(`/api/member/${this.member.id}`, data)
                    .then(() => {
                        window.location.href = '/admin'
                    })
                    .catch(({response}) => {
                        if (response) {
                            this.errorState = { ...response.data.errors, ...response.data._error }
                        }
                    })
            }
        },
    })
</script>
@endsection
