@extends('admin.layouts')
@section('content')
@verbatim
    <div class="row">
        <div class="col-lg-6 mb-4">
            <!-- Illustrations -->
            <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">
                        Member name {{ member.firstNameEN }} {{ member.lastNameEN }}
                    </h6>
                    <h6 class="m-0 font-weight-bold text-primary">
                        Count Modify {{ member.logs.length }}
                    </h6>
                </div>
                <div class="card-body" v-if="member.logs.length">
                    <div v-for="(log, key) in member.logs" :key="key">
                        <card-list
                            :icon-active="log.id === active"
                            class="mb-2"
                            :data="log"
                            :action-by="log.action_by?.name || `${member.firstNameEN} ${member.lastNameEN}`"
                            @click="viewLogs"
                        >
                        </card-list>
                    </div>
                </div>
                <div class="card-body text-center" v-else>
                    <small class="m-0">Empty Logs</small>
                </div>
            </div>
        </div>
        <div class="col-lg-6 mb-4">
            <!-- Illustrations -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Logs Details</h6>
                </div>
                <div v-if="logsDetail.length" class="card-body">
                    <base-table :data="logsDetail" :columns="columns">
                        <template slot-scope="{ row }">
                            <td scope="row">{{ row.fields }}</td>
                            <td scope="row" class="text-success">{{ row.original }}</td>
                            <td scope="row" class="text-danger" style="text-decoration: line-through">{{ row.old }}</td>
                        </template>
                    </base-table>
                </div>
                <div class="card-body text-center" v-else>
                    <small class="m-0">Empty Logs</small>
                </div>
            </div>
        </div>
    </div>
@endverbatim
@endsection

@section('script')
<script>
    new Vue({
        el: '#app',
        data() {
            return {
                member: {!! $member->toJson() !!},
                columns: [
                    'field',
                    'original',
                    'old'
                ],
                active: {!! count($member->logs) ? $member->logs[0]->id : 0 !!},
                logsDetail: {!! count($member->logs) ? $member->logs[0]->detail->toJson() : '[]' !!},
            }
        },
        methods: {
            viewLogs(id) {
                this.logsDetail = this.member.logs.find(log => log.id === id).detail
                this.active = id
            }
        },
    })
</script>
@endsection
